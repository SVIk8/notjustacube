﻿using UnityEngine;

public class FPSController : MonoBehaviour {
	public int targetFPS = 144;

	private void Awake() {
		Application.targetFrameRate = targetFPS;
	}
	private void OnGUI() {
		GUILayout.Label((1f / Time.deltaTime).ToString());
	}
}
