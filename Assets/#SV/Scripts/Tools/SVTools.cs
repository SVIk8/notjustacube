﻿using UnityEngine;

public class SVTools : MonoBehaviour {
	public static Vector3 DirToVector(DirAxis _axis, Transform _t) {
		switch (_axis) {
			case DirAxis.Right: return _t.right;
			case DirAxis.Up: return _t.up;
			case DirAxis.Forward: return _t.forward;
			case DirAxis.Left: return -_t.right;
			case DirAxis.Down: return -_t.up;
			case DirAxis.Back: return -_t.forward;
		}
		return Vector3.zero;
	}

	public static Vector3 DirToVector(DirAxis _axis) {
		switch (_axis) {
			case DirAxis.Right: return Vector3.right;
			case DirAxis.Up: return Vector3.up;
			case DirAxis.Forward: return Vector3.forward;
			case DirAxis.Left: return -Vector3.right;
			case DirAxis.Down: return -Vector3.up;
			case DirAxis.Back: return -Vector3.forward;
		}
		return Vector3.zero;
	}
}
