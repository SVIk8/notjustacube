﻿using UnityEngine;

public class StickCubeColliderGenerator : MonoBehaviour {
	[HideInInspector]
	public float colWidth = 0.1f;
	[HideInInspector]
	public bool
		rightCol = true,
		upCol = true,
		forwCol = true,
		leftCol = true,
		downCol = true,
		backCol = true;
}
