﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerExit : MonoBehaviour {
	public UnityEventCollider onTriggerExit;

	private void OnTriggerExit(Collider _collider) {
		onTriggerExit.Invoke(_collider);
	}
}
