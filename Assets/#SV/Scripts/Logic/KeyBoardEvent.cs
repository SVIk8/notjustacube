﻿using UnityEngine;
using UnityEngine.Events;

public class KeyBoardEvent : MonoBehaviour {
	public KeyCode key;
	public bool d_pressed;
	private bool isPressed;
	public UnityEvent onKeyDown, onKeyUp;

	private void Update() {
		if (Input.GetKeyDown(key) || d_pressed && !isPressed) {
			isPressed = d_pressed = true;
			onKeyDown.Invoke();
		}
		if (Input.GetKeyUp(key) || !d_pressed && isPressed) {
			isPressed = d_pressed = false;
			onKeyUp.Invoke();
		}
	}
}
