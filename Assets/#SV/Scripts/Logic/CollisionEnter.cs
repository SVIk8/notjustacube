﻿using UnityEngine;

public class CollisionEnter : MonoBehaviour {
	public UnityEventCollision onCollisionEnter;

	private void OnCollisionEnter(Collision _collision) {
		onCollisionEnter.Invoke(_collision);
	}
}
