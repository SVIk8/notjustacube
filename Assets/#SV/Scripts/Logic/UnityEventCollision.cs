﻿using UnityEngine;
using UnityEngine.Events;
using System;

[Serializable]
public class UnityEventCollision : UnityEvent<Collision> { }