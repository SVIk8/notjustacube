﻿using System.Collections.Generic;
using UnityEngine;

public class StateMachine {
	public List<StateEvents> states;
	public SimpleEventInt onStateChanged;

	public int
		curState = 0,
		stateId = 0;

	public StateMachine(int _statesCount) {
		states = new List<StateEvents>();
		for (int i = _statesCount; i > 0; --i)
			states.Add(new StateEvents());
	}

	public void ChangeTo(int _newState) {
		if (_newState >= states.Count || _newState == curState) return;
		states[curState].onEnd?.Invoke();
		curState = _newState;
		++stateId;
		onStateChanged?.Invoke(curState);
		states[curState].onStart?.Invoke();
		Debug.Log("State now is: " + curState.ToString());
	}
}

public class StateEvents {
	public SimpleEvent onStart, onEnd;
}
