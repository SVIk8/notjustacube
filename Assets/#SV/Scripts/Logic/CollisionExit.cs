﻿using UnityEngine;

public class CollisionExit : MonoBehaviour {
	public UnityEventCollision onCollisionEnter;

	private void OnCollisionExit(Collision _collision) {
		onCollisionEnter.Invoke(_collision);
	}
}
