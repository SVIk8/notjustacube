﻿public delegate void SimpleEvent();
public delegate void SimpleEventInt(int _num);
public delegate void SimpleEventLabel(Label _label);
