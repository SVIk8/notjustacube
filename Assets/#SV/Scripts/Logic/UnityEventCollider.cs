﻿using UnityEngine;
using UnityEngine.Events;
using System;

[Serializable]
public class UnityEventCollider : UnityEvent<Collider> { }
