﻿using UnityEngine;

public class Label : MonoBehaviour {
	public LabelType[] labels;
	public SimpleEventLabel onLabelDisabled;

	public bool HasLabel(LabelType _label) {
		for (int i = labels.Length - 1; i >= 0; --i)
			if (labels[i] == _label)
				return true;
		return false;
	}

	private void OnDisable() {
		onLabelDisabled?.Invoke(this);
	}
}
