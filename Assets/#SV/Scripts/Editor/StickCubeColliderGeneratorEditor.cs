﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(StickCubeColliderGenerator))]
public class StickColliderGeneratorEditor : Editor {
	public override void OnInspectorGUI() {
		StickCubeColliderGenerator theScript = (StickCubeColliderGenerator)target;
		GUILayout.BeginHorizontal();
		theScript.rightCol = GUILayout.Toggle(theScript.rightCol, "Right", GUILayout.Width(70f));
		theScript.upCol = GUILayout.Toggle(theScript.upCol, "Up", GUILayout.Width(70f));
		theScript.forwCol = GUILayout.Toggle(theScript.forwCol, "Forward", GUILayout.Width(70f));
		theScript.colWidth = float.Parse(GUILayout.TextField(theScript.colWidth.ToString(), GUILayout.Width(70f)));
		GUILayout.Label("Collider's width");
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		theScript.leftCol = GUILayout.Toggle(theScript.leftCol, "Left", GUILayout.Width(70f));
		theScript.downCol = GUILayout.Toggle(theScript.downCol, "Down", GUILayout.Width(70f));
		theScript.backCol = GUILayout.Toggle(theScript.backCol, "Back", GUILayout.Width(70f));
		if (GUILayout.Button("Clear", GUILayout.MinWidth(10f))) {
			CubeSticker[] stickers = theScript.GetComponentsInChildren<CubeSticker>();
			for (int i = stickers.Length - 1; i >= 0; --i)
				DestroyImmediate(stickers[i].gameObject);
		}
		if (GUILayout.Button("Generate", GUILayout.MinWidth(10f))) {
			if (theScript.rightCol) {
				GameObject newColObj = EmptyGO(theScript.transform);
				newColObj.name = theScript.name + "StickColliderRight";
				newColObj.transform.localPosition = new Vector3(.5f, 0f, 0f);
				newColObj.transform.localScale = new Vector3(theScript.colWidth / newColObj.transform.parent.lossyScale.x, 1f, 1f);
				newColObj.AddComponent<BoxCollider>().center = new Vector3(-.5f, 0f, 0f);
				newColObj.AddComponent<CubeSticker>().side = DirAxis.Right;
			}
			if (theScript.upCol) {
				GameObject newColObj = EmptyGO(theScript.transform);
				newColObj.name = theScript.name + "StickColliderUp";
				newColObj.transform.localPosition = new Vector3(0f, .5f, 0f);
				newColObj.transform.localScale = new Vector3(1f, theScript.colWidth / newColObj.transform.parent.lossyScale.y, 1f);
				newColObj.AddComponent<BoxCollider>().center = new Vector3(0f, -.5f, 0f);
				newColObj.AddComponent<CubeSticker>().side = DirAxis.Up;
			}
			if (theScript.forwCol) {
				GameObject newColObj = EmptyGO(theScript.transform);
				newColObj.name = theScript.name + "StickColliderForward";
				newColObj.transform.localPosition = new Vector3(0f, 0f, .5f);
				newColObj.transform.localScale = new Vector3(1f, 1f, theScript.colWidth / newColObj.transform.parent.lossyScale.z);
				newColObj.AddComponent<BoxCollider>().center = new Vector3(0f, 0f, -.5f);
				newColObj.AddComponent<CubeSticker>().side = DirAxis.Forward;
			}
			if (theScript.leftCol) {
				GameObject newColObj = EmptyGO(theScript.transform);
				newColObj.name = theScript.name + "StickColliderLeft";
				newColObj.transform.localPosition = new Vector3(-.5f, 0f, 0f);
				newColObj.transform.localScale = new Vector3(theScript.colWidth / newColObj.transform.parent.lossyScale.x, 1f, 1f);
				newColObj.AddComponent<BoxCollider>().center = new Vector3(.5f, 0f, 0f);
				newColObj.AddComponent<CubeSticker>().side = DirAxis.Left;
			}
			if (theScript.downCol) {
				GameObject newColObj = EmptyGO(theScript.transform);
				newColObj.name = theScript.name + "StickColliderDown";
				newColObj.transform.localPosition = new Vector3(0f, -.5f, 0f);
				newColObj.transform.localScale = new Vector3(1f, theScript.colWidth / newColObj.transform.parent.lossyScale.y, 1f);
				newColObj.AddComponent<BoxCollider>().center = new Vector3(0f, .5f, 0f);
				newColObj.AddComponent<CubeSticker>().side = DirAxis.Down;
			}
			if (theScript.backCol) {
				GameObject newColObj = EmptyGO(theScript.transform);
				newColObj.name = theScript.name + "StickColliderBack";
				newColObj.transform.localPosition = new Vector3(0f, 0f, -.5f);
				newColObj.transform.localScale = new Vector3(1f, 1f, theScript.colWidth / newColObj.transform.parent.lossyScale.z);
				newColObj.AddComponent<BoxCollider>().center = new Vector3(0f, 0f, .5f);
				newColObj.AddComponent<CubeSticker>().side = DirAxis.Back;
			}
		}
		GUILayout.EndHorizontal();
	}

	private GameObject EmptyGO(Transform _parent) {
		GameObject emptyGO = new GameObject();
		emptyGO.transform.parent = _parent;
		emptyGO.transform.localRotation = Quaternion.identity;
		return emptyGO;
	}
}
