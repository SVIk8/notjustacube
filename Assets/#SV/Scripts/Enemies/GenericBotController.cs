﻿using System.Collections;
using UnityEngine;

public class GenericBotController : MonoBehaviour {
	public StraightFlatMover mover;
	public BotVision vision;
	private StateMachine
		actionStates,
		behaviourStates;

	// TODO: Выпилить это в отдельный класс, если будет разрастаться.
	public MoveZone walkZone;
	private Transform curTarget;

	private void Awake() {
		actionStates = new StateMachine((int)BotActionState.E_Count);
		behaviourStates = new StateMachine((int)GenericBotBehaviourState.E_Count);
	}

	private void Start() {
		mover.SubscribeOnStop(() => { actionStates.ChangeTo((int)BotActionState.Wait); });

		behaviourStates.states[(int)GenericBotBehaviourState.Idle].onStart += () => { actionStates.ChangeTo((int)BotActionState.Wait); };
		behaviourStates.states[(int)GenericBotBehaviourState.Mine].onStart += () => { actionStates.ChangeTo((int)BotActionState.Walk); };
		behaviourStates.states[(int)GenericBotBehaviourState.Attack].onStart += () => { actionStates.ChangeTo((int)BotActionState.Walk); };

		actionStates.states[(int)BotActionState.Wait].onStart += () => { StartCoroutine(DelayedSetState((int)BotActionState.Walk, 5f)); };
		actionStates.states[(int)BotActionState.Walk].onStart += () => {
			switch (behaviourStates.curState) {
				case (int)GenericBotBehaviourState.Idle: mover.MoveTo(walkZone.GetRandomPoint()); break;
				default: mover.MoveTo(curTarget); break;
			}
		};
		actionStates.states[(int)BotActionState.Walk].onEnd += () => { mover.StopMove(); };

		behaviourStates.ChangeTo((int)GenericBotBehaviourState.Idle);

		vision.onLabelOutOffVision += UpdateTarget;
		vision.onNewLabelInVision += UpdateTarget;
	}

	private IEnumerator DelayedSetState(int _newState, float _delay, bool thisStateOnly = true) {
		int wasState = actionStates.stateId;
		yield return new WaitForSeconds(_delay);
		if (thisStateOnly && wasState == actionStates.stateId)
			actionStates.ChangeTo(_newState);
	}

	// TODO: Выпилить поведения в отдельный класс, если будут разрастаться.
	private void UpdateTarget(Label _labelChanged) {
		int targetTypeId = -1;
		for (int i = 0; i < vision.inTrigger.Length; ++i)
			if (vision.inTrigger[i].Count > 0) {
				targetTypeId = i;
				break;
			}
		if (targetTypeId == -1) {
			mover.followTarget = curTarget = null;
			behaviourStates.ChangeTo((int)GenericBotBehaviourState.Idle);
		} else {
			mover.followTarget = curTarget = vision.inTrigger[targetTypeId][0].Item1.transform;
			if (vision.labelTypes[targetTypeId] == LabelType.Player)
				behaviourStates.ChangeTo((int)GenericBotBehaviourState.Attack);
			else
				behaviourStates.ChangeTo((int)GenericBotBehaviourState.Mine);
		}
	}
}
