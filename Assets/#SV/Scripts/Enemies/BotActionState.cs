﻿public enum BotActionState {
	None,
	Wait,
	Walk,
	Run,
	Jump,
	Attack,
	E_Count
}
