﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class BotVision : MonoBehaviour {
	public LabelType[] labelTypes;
	private Label lastLabel;

	// Окей, разберём это дерьмо ещё раз.
	// Вообще это Label'ы в области наблюдения.
	// Сначала идёт сортировка по заданным LabelType'ам.
	// Внутри список всех подходящих Label'ов. Да, элементы дублируются для разных LabelType'ов.
	// Для каждого Label'а впару ставится список Collider'ов, которые находятся в области наблюдения.
	// TODO: Возможно, добавить ещё элемент к каждому Label'у для определения прямой видимости через RayCast.
	public List<Tuple<Label, List<Collider>>>[] inTrigger;

	public SimpleEventLabel onNewLabelInVision, onLabelOutOffVision;

	private void Awake() {
		inTrigger = new List<Tuple<Label, List<Collider>>>[labelTypes.Length];
		for (int i = labelTypes.Length - 1; i >= 0; --i)
			inTrigger[i] = new List<Tuple<Label, List<Collider>>>();
	}

	public void TriggerEntered(Collider _collider) {
		lastLabel = _collider.GetComponentInParent<Label>();
		if (lastLabel == null)
			return;

		for (int i = labelTypes.Length - 1; i >= 0; --i)
			if (lastLabel.HasLabel(labelTypes[i])) {
				int j;
				for (j = inTrigger[i].Count - 1; j >= 0; --j)
					if (inTrigger[i][j].Item1 == lastLabel)
						if (inTrigger[i][j].Item2.Contains(_collider)) {
							Debug.LogWarning($"Collider {_collider.gameObject.name} is already in vision", gameObject);
							break;
						} else {
							inTrigger[i][j].Item2.Add(_collider);
							break;
						}
				if (j == -1) {
					inTrigger[i].Add(new Tuple<Label, List<Collider>>(lastLabel, new List<Collider> { _collider }));
					onNewLabelInVision?.Invoke(lastLabel);
					lastLabel.onLabelDisabled += LabelDisabled;
				}
			}
	}

	public void TriggerExited(Collider _collider) {
		lastLabel = _collider.GetComponentInParent<Label>();
		if (lastLabel == null)
			return;

		for (int i = labelTypes.Length - 1; i >= 0; --i)
			if (lastLabel.HasLabel(labelTypes[i])) {
				int j;
				for (j = inTrigger[i].Count - 1; j >= 0; --j)
					if (inTrigger[i][j].Item1 == lastLabel) {
						if (inTrigger[i][j].Item2.Contains(_collider)) {
							inTrigger[i][j].Item2.Remove(_collider);
							if (inTrigger[i][j].Item2.Count == 0) {
								inTrigger[i].RemoveAt(j);
								onLabelOutOffVision?.Invoke(lastLabel);
								lastLabel.onLabelDisabled -= LabelDisabled;
							}
						} else
							Debug.LogWarning($"Collider {_collider.gameObject} is already out of vision", gameObject);
						return;
					}
				if (j == -1)
					Debug.LogWarning($"Label {lastLabel.gameObject.name} is already out of vision", gameObject);
			}
	}

	// TODO: Проверить подписывается ли несколько раз для разных LabelType'ов.
	private void LabelDisabled(Label _label) {
		for (int i = labelTypes.Length - 1; i >= 0; --i)
			if (_label.HasLabel(labelTypes[i])) {
				int j;
				for (j = inTrigger[i].Count - 1; j >= 0; --j)
					if (inTrigger[i][j].Item1 == _label) {
						inTrigger[i].RemoveAt(j);
						onLabelOutOffVision?.Invoke(_label);
						// TODO: Поменять на return, в случае если нужно будет отписаться за каждый LabelType.
						break;
					}
				if (j == -1)
					Debug.LogWarning($"Label {_label.gameObject.name} is already out of vision", gameObject);
			}

		_label.onLabelDisabled -= LabelDisabled;
	}
}
