﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class BotVisionAgent : MonoBehaviour {
	public List<Tuple <BotVision, List<Collider>>> visors = new List<Tuple<BotVision, List<Collider>>>();

	public void AddCollider(Collider _collider, BotVision _visor) {
		int visorId = visors.Count - 1;
		for (; visorId >= 0 && visors[visorId].Item1 != _visor; --visorId) ;
		if (visorId == -1)
			visors.Add(new Tuple<BotVision, List<Collider>>(_visor, new List<Collider>() { _collider }));
		else if (visors[visorId].Item2.Contains(_collider))
			Debug.LogWarning($"BotVisionAgent already watches at collider for {_visor.gameObject.name}", gameObject);
		else
			visors[visorId].Item2.Add(_collider);
	}

	private void OnDisable() {
		for (int i = visors.Count - 1; i >= 0; --i) {
			for (int j = visors[i].Item2.Count - 1; j >= 0; --j) {
				visors[i].Item1
			}
		}
	}
}
