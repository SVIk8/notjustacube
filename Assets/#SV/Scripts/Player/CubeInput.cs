﻿using UnityEngine;

public class CubeInput : MonoBehaviour {
	public KeyCode
		k_w = KeyCode.W,
		k_a = KeyCode.A,
		k_s = KeyCode.S,
		k_d = KeyCode.D,
		k_space = KeyCode.Space,
		k_lctrl = KeyCode.LeftControl;

	public static bool
		s_w,
		s_a,
		s_s,
		s_d,
		s_space,
		s_lctrl;
	public bool
		d_w,
		d_a,
		d_s,
		d_d,
		d_space,
		d_lctrl;

	public static SimpleEvent
		WDown, WUp,
		ADown, AUp,
		SDown, SUp,
		DDown, DUp,
		SpaceDown, SpaceUp,
		CtrlDown, CtrlUp;

	private void Update() {
		if (Input.GetKeyDown(k_w) || d_w && !s_w) {
			d_w = s_w = true;
			WDown?.Invoke();
		}
		if (Input.GetKeyUp(k_w) || !d_w && s_w) {
			d_w = s_w = false;
			WUp?.Invoke();
		}

		if (Input.GetKeyDown(k_a) || d_a && !s_a) {
			d_a = s_a = true;
			ADown?.Invoke();
		}
		if (Input.GetKeyUp(k_a) || !d_a && s_a) {
			d_a = s_a = false;
			AUp?.Invoke();
		}

		if (Input.GetKeyDown(k_s) || d_s && !s_s) {
			d_s = s_s = true;
			SDown?.Invoke();
		}
		if (Input.GetKeyUp(k_s) || !d_s && s_s) {
			d_s = s_s = false;
			SUp?.Invoke();
		}

		if (Input.GetKeyDown(k_d) || d_d && !s_d) {
			d_d = s_d = true;
			DDown?.Invoke();
		}
		if (Input.GetKeyUp(k_d) || !d_d && s_d) {
			d_d = s_d = false;
			DUp?.Invoke();
		}

		if (Input.GetKeyDown(k_space) || d_space && !s_space) {
			d_space = s_space = true;
			SpaceDown?.Invoke();
		}
		if (Input.GetKeyUp(k_space) || !d_space && s_space) {
			d_space = s_space = false;
			SpaceUp?.Invoke();
		}

		if (Input.GetKeyDown(k_lctrl) || d_lctrl && !s_lctrl) {
			d_lctrl = s_lctrl = true;
			CtrlDown?.Invoke();
		}
		if (Input.GetKeyUp(k_lctrl) || !d_lctrl && s_lctrl) {
			d_lctrl = s_lctrl = false;
			CtrlUp?.Invoke();
		}
	}
}
