﻿using UnityEngine;

public class CameraMove : MonoBehaviour {
	public Transform
		followTarget,
		cameraRotationY,
		cameraRotationX;

	public float
		followAprox = 1f,
		lookAprox = 1f,
		sensitivity = 50f,
		rotationLock = 60f;

	public Vector3 mousePos;

	private float 
		xCur, yCur;

	private void Awake() {
		Cursor.lockState = CursorLockMode.Locked;
	}

	private void FixedUpdate() {
		transform.position = Vector3.Lerp(transform.position, followTarget.position, followAprox * Time.fixedDeltaTime);
		transform.rotation = Quaternion.Lerp(transform.rotation, followTarget.rotation, lookAprox * Time.fixedDeltaTime);

		mousePos.x = Input.GetAxis("Mouse X");
		mousePos.y = Input.GetAxis("Mouse Y");

		xCur = Vector3.SignedAngle(cameraRotationX.parent.forward, cameraRotationX.forward, cameraRotationX.parent.right);
		cameraRotationX.localEulerAngles = new Vector3(xCur - sensitivity * mousePos.y * Time.fixedDeltaTime, 0f, 0f);

		yCur = Vector3.SignedAngle(cameraRotationY.parent.forward, cameraRotationY.forward, cameraRotationY.parent.up);
		yCur = Mathf.Clamp(yCur + sensitivity * mousePos.x * Time.fixedDeltaTime, -rotationLock, rotationLock);
		cameraRotationY.localEulerAngles = new Vector3(0f, yCur, 0f);
	}
}
