﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveZone : MonoBehaviour {
	public Vector3 GetRandomPoint() {
		return transform.TransformPoint(new Vector3(
			Random.Range(-.5f, .5f),
			Random.Range(-.5f, .5f),
			Random.Range(-.5f, .5f)));
		//Random.Range(-transform.localScale.x / 2, transform.localScale.x / 2),
		//	Random.Range(-transform.localScale.y / 2, transform.localScale.y / 2),
		//	Random.Range(-transform.localScale.z / 2, transform.localScale.z / 2)));
	}
}
