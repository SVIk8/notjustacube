﻿using UnityEngine;

public class StraightFlatMover : MonoBehaviour, IMover {
	public Transform movable;
	public float
		rotationVelocity,
		moveVelocity;

	//public bool d_IsMoving;
	private bool isMoving;
	private Vector3
		targetPoint, localTargetPoint;
	private float angleDif;
	public Transform followTarget;

	public Animator animator;

	private SimpleEvent hasStopped;

	//private void Update() {
	//	if (d_IsMoving != isMoving)
	//		if (d_IsMoving)
	//			ContinueMove();
	//		else
	//			StopMove();
	//}

	private void FixedUpdate() {
		if (!isMoving) return;
		if (followTarget != null) UpdateTarget();

		localTargetPoint = movable.InverseTransformPoint(targetPoint);
		localTargetPoint.y = 0f;
		targetPoint = movable.TransformPoint(localTargetPoint);
		angleDif = Vector3.SignedAngle(Vector3.forward, localTargetPoint, Vector3.up);
		if (Mathf.Abs(angleDif) > .1f)
			if (rotationVelocity * Time.fixedDeltaTime > Mathf.Abs(angleDif))
				movable.rotation = Quaternion.AngleAxis(angleDif, movable.up) * movable.rotation;
			else
				movable.rotation = Quaternion.AngleAxis(Mathf.Sign(angleDif) * rotationVelocity * Time.fixedDeltaTime, movable.up) * movable.rotation;

		if ((movable.position - targetPoint).magnitude < moveVelocity * Time.fixedDeltaTime) {
			movable.position = targetPoint;
			StopMove();
		} else
			movable.position += moveVelocity * (targetPoint - movable.position).normalized * Time.fixedDeltaTime;
	}

	public void MoveTo(Vector3 _target) {
		isMoving = true;
		animator.SetInteger("State", (int)GeneralAnimationBotStates.Walk);
		targetPoint = _target;
		followTarget = null;
	}

	public void MoveTo(Transform _target) {
		isMoving = true;
		animator.SetInteger("State", (int)GeneralAnimationBotStates.Walk);
		followTarget = _target;
	}

	public void ContinueMove() {
		isMoving = true;
		animator.SetInteger("State", (int)GeneralAnimationBotStates.Walk);
	}

	private void UpdateTarget() {
		targetPoint = followTarget.position;
	}

	public void StopMove() {
		if (!isMoving) return;
		isMoving = false;
		animator.SetInteger("State", (int)GeneralAnimationBotStates.Idle);
		hasStopped?.Invoke();
	}

	private void OnDrawGizmos() {
		Gizmos.color = Color.red;
		Gizmos.DrawSphere(targetPoint, 0.1f);
	}

	public void SubscribeOnStop(SimpleEvent _toDo) {
		hasStopped += _toDo;
	}
}
