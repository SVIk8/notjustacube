﻿using UnityEngine;

public class CubeMove : MonoBehaviour, IVelocitier {
	public Transform
		target,
		lookPoint;
	public MoveStick moveStick;

	public float
		normalMoveVelocity = 10f;
	public AnimationCurve acceleration;
	public float
		normalAngularVelocity = 180f,
		normalAngularAcceleration = 360f,
		jumpPower = 2f,
		jumpBoost = 2f,
		gravity = 2f,
		maxVelocity = 20f,
		normalAccelerationMultiplier = 1f,
		airAccelerationMultiplier = 0.1f;

	public Vector3 linearVelocity, linearAcceleration;
	private float
		jumpFuel,
		angularVelocity,
		angularAcceleration,
		accelerationMultiplier,
		timeStep;
	private bool
		hasDrop;

	private void Awake() {
		RefreshFuel();
		CubeInput.ADown += ProcessADown;
		CubeInput.DDown += ProcessDDown;
		CubeInput.SpaceDown += ProcessSpaceDown;
		CubeInput.SpaceUp += ProcessSpaceUp;
		CubeInput.CtrlDown += ProcessCtrlDown;
	}

	public MoveStateType State {
		get {
			return state;
		}
		set {
			state = value;
			switch (value) {
				case MoveStateType.Standing:
					RefreshFuel();
					break;
				case MoveStateType.Sticked:
					RefreshFuel();
					angularVelocity = 0f;
					break;
				case MoveStateType.Hanging:
					RefreshFuel();
					angularVelocity = 0f;
					break;
				case MoveStateType.Falling:
					accelerationMultiplier = airAccelerationMultiplier;
					break;
				case MoveStateType.Jumping:
					accelerationMultiplier = airAccelerationMultiplier;
					break;
			}
		}
	}
	private MoveStateType state = MoveStateType.Falling;

	private void FixedUpdate() {
		timeStep = Mathf.Clamp(Time.fixedDeltaTime, 0f, 0.05f);
		// Место для влияния внешних силовых полей
		linearAcceleration = -gravity * Vector3.up;
		angularAcceleration = 0f;

		linearVelocity = target.InverseTransformVector(linearVelocity);
		linearAcceleration = target.InverseTransformVector(linearAcceleration);
		switch (State) {
			case MoveStateType.Standing:
				linearAcceleration /= 5f;
				ReduceStickedMove(10f);
				MainDirMove();
				SideDirSlow();
				SideRotation();
				break;
			case MoveStateType.Sticked:
				linearAcceleration /= 10f;
				ReduceStickedMove(10f);
				SideDirSlow();
				MainDirMove();
				break;
			case MoveStateType.Hanging:
				linearAcceleration /= 5f;
				ReduceStickedMove(10f);
				MainDirSlow();
				SideDirSlow();
				VericalSlow();
				break;
			case MoveStateType.Jumping:
				MainDirMove();
				SideRotation();

				linearAcceleration += jumpFuel * Vector3.up;
				jumpFuel = Mathf.Clamp(jumpFuel - jumpBoost * timeStep * 4, 0f, float.PositiveInfinity);
				if (jumpFuel == 0)
					State = MoveStateType.Falling;
				break;
			case MoveStateType.Falling:
				MainDirMove();
				SideRotation();
				break;
		}
		linearAcceleration = target.TransformVector(linearAcceleration);
		linearVelocity = target.TransformVector(linearVelocity);

		linearVelocity += linearAcceleration * timeStep;
		linearVelocity = linearVelocity.normalized * Mathf.Clamp(linearVelocity.magnitude, 0f, maxVelocity);
		target.position += linearVelocity * timeStep;

		angularVelocity = Mathf.Clamp(angularVelocity + angularAcceleration * timeStep, -normalAngularVelocity, normalAngularVelocity);
		target.rotation = Quaternion.AngleAxis(-angularVelocity * timeStep, target.up) * target.rotation;
	}

	// Методы работают со скоростью и линейным ускорением в системе отсчёта двигающегося тела
	#region LocalCoordinateSystem
	private void ReduceStickedMove(float _stickness) {
		if (moveStick.stickers[(int)DirAxis.Right].Count > 0) {
			if (linearVelocity.x > 0f)
				linearVelocity.x = 0f;
			if (linearAcceleration.x > -_stickness)
				linearAcceleration.x = 0;
			else
				linearAcceleration.x += _stickness;
		}
		if (moveStick.stickers[(int)DirAxis.Up].Count > 0) {
			if (linearVelocity.y > 0f)
				linearVelocity.y = 0f;
			if (linearAcceleration.y > -_stickness)
				linearAcceleration.y = 0;
			else
				linearAcceleration.y += _stickness;
		}
		if (moveStick.stickers[(int)DirAxis.Forward].Count > 0) {
			if (linearVelocity.z > 0f)
				linearVelocity.z = 0f;
			if (linearAcceleration.z > -_stickness)
				linearAcceleration.z = 0;
			else
				linearAcceleration.z += _stickness;
		}
		if (moveStick.stickers[(int)DirAxis.Left].Count > 0) {
			if (linearVelocity.x < 0f)
				linearVelocity.x = 0f;
			if (linearAcceleration.x < _stickness)
				linearAcceleration.x = 0;
			else
				linearAcceleration.x -= _stickness;
		}
		if (moveStick.stickers[(int)DirAxis.Down].Count > 0) {
			if (linearVelocity.y < 0f)
				linearVelocity.y = 0f;
			if (linearAcceleration.y < _stickness)
				linearAcceleration.y = 0;
			else
				linearAcceleration.y -= _stickness;
		}
		if (moveStick.stickers[(int)DirAxis.Back].Count > 0) {
			if (linearVelocity.z < 0f)
				linearVelocity.z = 0f;
			if (linearAcceleration.z < _stickness)
				linearAcceleration.z = 0;
			else
				linearAcceleration.z -= _stickness;
		}
	}

	// Прямое ускорение
	private void MainDirMove() {
		bool
			movesForward = CubeInput.s_w && moveStick.stickers[(int)DirAxis.Forward].Count <= 0,
			movesBack = CubeInput.s_s && moveStick.stickers[(int)DirAxis.Back].Count <= 0;
		if (movesForward ^ movesBack)
			if (movesForward)
				if (linearVelocity.z > normalMoveVelocity)
					linearAcceleration -= Vector3.forward * acceleration.Evaluate(2 * normalMoveVelocity - linearVelocity.z) * accelerationMultiplier;
				else
					linearAcceleration += Vector3.forward * acceleration.Evaluate(linearVelocity.z) * accelerationMultiplier;
			else if (linearVelocity.z < -normalMoveVelocity)
				linearAcceleration += Vector3.forward * acceleration.Evaluate(2 * normalMoveVelocity + linearVelocity.z) * accelerationMultiplier;
			else
				linearAcceleration -= Vector3.forward * acceleration.Evaluate(-linearVelocity.z) * accelerationMultiplier;
		else
			MainDirSlow();
	}
	private void MainDirSlow() {
		if (linearVelocity.z > 0)
			linearAcceleration -= Vector3.forward * Mathf.Clamp(acceleration.Evaluate(normalMoveVelocity - linearVelocity.z), 0f, linearVelocity.z / timeStep) * accelerationMultiplier;
		else
			linearAcceleration += Vector3.forward * Mathf.Clamp(acceleration.Evaluate(normalMoveVelocity + linearVelocity.z), 0f, -linearVelocity.z / timeStep) * accelerationMultiplier;
	}
	private void SideDirSlow() {
		if (linearVelocity.x > 0)
			linearAcceleration -= Vector3.right * Mathf.Clamp(acceleration.Evaluate(normalMoveVelocity - linearVelocity.x), 0f, linearVelocity.x / timeStep) * accelerationMultiplier;
		else
			linearAcceleration += Vector3.right * Mathf.Clamp(acceleration.Evaluate(normalMoveVelocity + linearVelocity.x), 0f, -linearVelocity.x / timeStep) * accelerationMultiplier;
	}
	private void VericalSlow() {
		if (linearVelocity.y > 0)
			linearAcceleration -= Vector3.up * Mathf.Clamp(acceleration.Evaluate(normalMoveVelocity - linearVelocity.y), 0f, linearVelocity.y / timeStep) * accelerationMultiplier;
		else
			linearAcceleration += Vector3.up * Mathf.Clamp(acceleration.Evaluate(normalMoveVelocity + linearVelocity.y), 0f, -linearVelocity.y / timeStep) * accelerationMultiplier;
	}
	#endregion

	private void SideRotation() {
		if (CubeInput.s_a ^ CubeInput.s_d)
			if (CubeInput.s_a)
				angularAcceleration += normalAngularAcceleration * accelerationMultiplier * accelerationMultiplier;
			else
				angularAcceleration -= normalAngularAcceleration * accelerationMultiplier * accelerationMultiplier;
		else if (angularVelocity > 0)
			angularAcceleration += Mathf.Clamp(-normalAngularAcceleration, -angularVelocity / timeStep, 0f) * accelerationMultiplier * accelerationMultiplier;
		else
			angularAcceleration += Mathf.Clamp(normalAngularAcceleration, 0f, -angularVelocity / timeStep) * accelerationMultiplier * accelerationMultiplier;
	}

	private void ProcessADown() {
		if (State == MoveStateType.Sticked)
			target.rotation = Quaternion.AngleAxis(-90f, target.up) * target.rotation;
	}
	private void ProcessDDown() {
		if (State == MoveStateType.Sticked)
			target.rotation = Quaternion.AngleAxis(90f, target.up) * target.rotation;
	}

	private void ProcessSpaceDown() {
		switch (State) {
			case MoveStateType.Standing:
				State = MoveStateType.Jumping;
				linearVelocity += jumpPower * target.up;
				break;
			case MoveStateType.Sticked:
				if (moveStick.stickers[(int)DirAxis.Forward].Count <= 0) break;
				lookPoint.rotation = Quaternion.AngleAxis(90f, target.right) * lookPoint.rotation;
				target.rotation = Quaternion.AngleAxis(-90f, target.right) * target.rotation;
				break;
			case MoveStateType.Hanging:
				// Если куб чем-то зацепился, то он разворачивается низом к поверхности
				int i;
				for (i = 0; i < (int)DirAxis.E_Count; ++i)
					if (moveStick.stickers[i].Count > 0)
						break;
				if (i >= (int)DirAxis.E_Count) break;

				float angleToCorrect = Vector3.Angle(target.up, moveStick.stickers[i][0].stickVector);
				if (angleToCorrect > 175f) {
					//TODO: Камера должна как-то спецэфично на это реагировать
					target.rotation = Quaternion.AngleAxis(angleToCorrect, target.forward) * target.rotation;
				} else {
					if (i % 3 == 2)
						lookPoint.rotation = Quaternion.AngleAxis(-angleToCorrect, Vector3.Cross(target.up, moveStick.stickers[i][0].stickVector)) * lookPoint.rotation;
					target.rotation = Quaternion.AngleAxis(angleToCorrect, Vector3.Cross(target.up, moveStick.stickers[i][0].stickVector)) * target.rotation;
				}
				break;
		}
	}
	private void ProcessSpaceUp() {
		if (State == MoveStateType.Jumping)
			State = MoveStateType.Falling;
	}

	private void ProcessCtrlDown() {
		if ((State == MoveStateType.Falling || State == MoveStateType.Jumping) && hasDrop) {
			linearVelocity += jumpPower * (-target.up) * 2;
			hasDrop = false;
		}
	}

	public void RefreshFuel() {
		hasDrop = true;
		jumpFuel = jumpBoost;
		accelerationMultiplier = normalAccelerationMultiplier;
	}

	public Vector3 GetVelocity() {
		return linearVelocity;
	}

	public void SetVelocity(Vector3 _velocity) {
		linearVelocity = _velocity;
	}
}
