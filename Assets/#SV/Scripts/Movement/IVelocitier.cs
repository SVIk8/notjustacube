﻿using UnityEngine;

public interface IVelocitier {
	Vector3 GetVelocity ();
	void SetVelocity(Vector3 _velocity);
}