﻿using UnityEngine;

public class Bouncer : MonoBehaviour {
	public float mass = 1000f;
	public GameObject impulserObject;
	private IVelocitier velocitier;

	private void Awake() {
		velocitier = impulserObject.GetComponent<IVelocitier>();
	}

	public void TryBounce(Collider _collider) {
		IBouncing bouncing = _collider.GetComponent<IBouncing>();
		if (bouncing == null) return;
		Vector3 newImpulse = bouncing.Bounce(mass * velocitier.GetVelocity());
		velocitier.SetVelocity(newImpulse / mass);
	}
}
