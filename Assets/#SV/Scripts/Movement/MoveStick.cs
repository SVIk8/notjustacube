﻿using System.Collections.Generic;
using UnityEngine;

public class MoveStick : MonoBehaviour {
	public List<CubeSticker>[] stickers = new List<CubeSticker>[(int)DirAxis.E_Count];
	public CubeMove cubeMove;
	public float
		stickSlow = 2f;

	private void Awake() {
		for (int i = 0; i < stickers.Length; ++i)
			stickers[i] = new List<CubeSticker>();
	}

	#region Particulat Cases of TryStick and TryUnstick for UnityEditor
	public void TryStickRight(Collider _collider) {
		CubeSticker cs = _collider.GetComponent<CubeSticker>();
		if (cs != null)
			TryStick(cs, DirAxis.Right);
	}
	public void TryUnstickRight(Collider _collider) {
		CubeSticker cs = _collider.GetComponent<CubeSticker>();
		if (cs != null)
			TryUnstick(cs, DirAxis.Right);
	}
	public void TryStickUp(Collider _collider) {
		CubeSticker cs = _collider.GetComponent<CubeSticker>();
		if (cs != null)
			TryStick(cs, DirAxis.Up);
	}
	public void TryUnstickUp(Collider _collider) {
		CubeSticker cs = _collider.GetComponent<CubeSticker>();
		if (cs != null)
			TryUnstick(cs, DirAxis.Up);
	}
	public void TryStickForward(Collider _collider) {
		CubeSticker cs = _collider.GetComponent<CubeSticker>();
		if (cs != null)
			TryStick(cs, DirAxis.Forward);
	}
	public void TryUnstickForward(Collider _collider) {
		CubeSticker cs = _collider.GetComponent<CubeSticker>();
		if (cs != null)
			TryUnstick(cs, DirAxis.Forward);
	}
	public void TryStickLeft(Collider _collider) {
		CubeSticker cs = _collider.GetComponent<CubeSticker>();
		if (cs != null)
			TryStick(cs, DirAxis.Left);
	}
	public void TryUnstickLeft(Collider _collider) {
		CubeSticker cs = _collider.GetComponent<CubeSticker>();
		if (cs != null)
			TryUnstick(cs, DirAxis.Left);
	}
	public void TryStickDown(Collider _collider) {
		CubeSticker cs = _collider.GetComponent<CubeSticker>();
		if (cs != null)
			TryStick(cs, DirAxis.Down);
	}
	public void TryUnstickDown(Collider _collider) {
		CubeSticker cs = _collider.GetComponent<CubeSticker>();
		if (cs != null)
			TryUnstick(cs, DirAxis.Down);
	}
	public void TryStickBack(Collider _collider) {
		CubeSticker cs = _collider.GetComponent<CubeSticker>();
		if (cs != null)
			TryStick(cs, DirAxis.Back);
	}
	public void TryUnstickBack(Collider _collider) {
		CubeSticker cs = _collider.GetComponent<CubeSticker>();
		if (cs != null)
			TryUnstick(cs, DirAxis.Back);
	}
	#endregion

	private void TryStick(CubeSticker _cs, DirAxis _side) {
		Vector3 cubeVector = SVTools.DirToVector(_side, cubeMove.target);
		if (Vector3.Angle(cubeVector, _cs.stickVector) < 135f) return;

		stickers[(int)_side].Add(_cs);
		//Debug.Log($"Stick by {_side.ToString()} to {_cs.transform.parent.name} Down: {stickers[(int)DirAxis.Down].Count} Forward: {stickers[(int)DirAxis.Forward].Count}");

		cubeMove.State = CountMoveState();
		if (cubeMove.State == MoveStateType.Hanging)
			cubeMove.linearVelocity  /= stickSlow;

		cubeMove.target.rotation = Quaternion.AngleAxis(Vector3.Angle(-cubeVector, _cs.stickVector), Vector3.Cross(-cubeVector, _cs.stickVector)) * cubeMove.target.rotation;
		cubeMove.target.position = _cs.GetStickPoint(cubeMove.target.position);
	}

	private void TryUnstick(CubeSticker _cs, DirAxis _side) {
		stickers[(int)_side].Remove(_cs);
		//Debug.Log($"Unstick by {_side.ToString()} from {_cs.transform.parent.name} Down: {stickers[(int)DirAxis.Down].Count} Forward: {stickers[(int)DirAxis.Forward].Count}");
		if (cubeMove.State != MoveStateType.Jumping)
			cubeMove.State = CountMoveState();
	}

	private MoveStateType CountMoveState() {
		if (stickers[(int)DirAxis.Down].Count > 0) {
			for (int i = 0; i < (int)DirAxis.E_Count; ++i) {
				if (i == (int)DirAxis.Down)
					continue;
				if (stickers[i].Count > 0)
					return MoveStateType.Sticked;
			}
			return MoveStateType.Standing;
		}
		for (int i = 0; i < (int)DirAxis.E_Count; ++i)
			if (stickers[i].Count > 0)
				return MoveStateType.Hanging;
		return MoveStateType.Falling;
	}
}
