﻿using UnityEngine;

public interface IBouncing {
	Vector3 Bounce(Vector3 _impulce);
}
