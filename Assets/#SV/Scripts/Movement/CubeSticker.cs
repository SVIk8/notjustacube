﻿using UnityEngine;

public class CubeSticker : MonoBehaviour {
	public DirAxis side;
	public Vector3 stickVector;
	private Vector3 answerPoint;

	private void Awake() {
		stickVector = SVTools.DirToVector(side, transform);
	}

	public Vector3 GetStickPoint(Vector3 _cubePoint, float _width = .5f) {
		answerPoint = transform.InverseTransformPoint(_cubePoint);
		switch (side) {
			case DirAxis.Right: answerPoint.x = _width / transform.lossyScale.x; break;
			case DirAxis.Up: answerPoint.y = _width / transform.lossyScale.y; break;
			case DirAxis.Forward: answerPoint.z = _width / transform.lossyScale.z; break;
			case DirAxis.Left: answerPoint.x = -_width / transform.lossyScale.x; break;
			case DirAxis.Down: answerPoint.y = -_width / transform.lossyScale.y; break;
			case DirAxis.Back: answerPoint.z = -_width / transform.lossyScale.z; break;
		}
		answerPoint = transform.TransformPoint(answerPoint);
		return answerPoint;
	}
}
