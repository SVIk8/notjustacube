﻿using UnityEngine;

public class CubePositionSetter : MonoBehaviour {
	public CubeMove target;
	public void SetCube() {
		target.target.position = transform.position;
		target.target.rotation = transform.rotation;
		target.linearAcceleration = Vector3.zero;
		target.linearVelocity = Vector3.zero;
	}
}
