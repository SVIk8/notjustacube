﻿using UnityEngine;

public class StaticBouncing : MonoBehaviour, IBouncing {
	public DirAxis bounceAxis;
	public float bouncePower = 1f;

	public Vector3 Bounce(Vector3 _impulce) {
		Vector3 localImpulce = transform.InverseTransformVector(_impulce);
		switch (bounceAxis) {
			case DirAxis.Right:
				if (localImpulce.x > 0f) return _impulce; break;
			case DirAxis.Up: if (localImpulce.y > 0f) return _impulce; break;
			case DirAxis.Forward: if (localImpulce.z > 0f) return _impulce; break;
			case DirAxis.Left: if (localImpulce.x < 0f) return _impulce; break;
			case DirAxis.Down: if (localImpulce.y < 0f) return _impulce; break;
			case DirAxis.Back: if (localImpulce.z < 0f) return _impulce; break;
		}
		switch ((int)bounceAxis % 3) {
			case 0: localImpulce.x *= -bouncePower; break;
			case 1: localImpulce.y *= -bouncePower; break;
			case 2: localImpulce.z *= -bouncePower; break;
		}
		return transform.TransformVector(localImpulce);
	}
}
