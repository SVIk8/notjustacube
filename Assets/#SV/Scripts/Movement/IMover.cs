﻿using UnityEngine;

public interface IMover {
	void SubscribeOnStop(SimpleEvent _toDo);
}
